FROM python:3.8

COPY . /home/app/notes
WORKDIR /home/app/notes

RUN pip install --upgrade pip

RUN pip install -r requirements.txt