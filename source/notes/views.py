from rest_framework import filters
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from source.notes.models import Note, Group
from source.notes.serializers import NoteSerializer, GroupSerializer


class NotesViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    search_fields = ['title']
    filter_backends = (filters.SearchFilter,)
    queryset = Note.objects.all()

    serializer_class = NoteSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GroupsViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    search_fields = ['title']
    filter_backends = (filters.SearchFilter,)
    queryset = Note.objects.all()

    serializer_class = GroupSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
